# Exercise 1.3 #

This is the directory for Exercise 1.3. To retrieve the exercise files, do 

    git fetch upstream
    git checkout -b exercise-1-3_Name upstream/exercise-1-3
    git push origin --set-upstream

To compile, do, for example:

    gfortran -fopenmp openmp1.f90 -o openmp1.x

To run with 4 threads (possible also on a single-core laptop), do:

    setenv OMP_NUM_THREADS 4          # for tcsh login shell
    export OMP_NUM_THREADS=4          # for bash login shell
    ./openmp1.x
