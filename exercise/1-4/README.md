# Exercise 1.4 #

This is the directory for Exercise 1.4. To retrieve the exercise files, do 

    git fetch upstream
    git checkout -b exercise-1-4_Name upstream/exercise-1-4
    git push origin --set-upstream

To compile, do:

    gfortran -fopenmp timer_mod.f90 objects.f90 -o objects.x

For exercise instructions, see the Absalon assignment page
