# Exercise 1.2 #

This is the directory for Exercise 1.2.  To retrieve the exercise files, do 

     git fetch upstream
     git checkout -b exercise-1-2_Name upstream/exercise-1-2
     git push origin --set-upstream

To compile the Fortran programs, do

     gfortran classical.f90 -o classical.x
     gfortran modules.f90 -o modules.x
     gfortran -fopenmp timer_mod.f90 objects.f90 -o objects.x

(the objects.f90 uses OpenMP, therefore it needs the -fopenmp option)
