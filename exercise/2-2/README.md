# Exercise 2.2 #

This is the directory for Exercise 2.2.  To retrieve the exercise files, do 

    git fetch upstream
    git checkout -b exercise-2-2_YourName upstream/exercise-2-2
    git push origin --set-upstream

To compiler the code, do

    make

For exercise details, see the Absalon assignment text
