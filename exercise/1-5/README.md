# Exercise 1.5 #

This is the directory for Exercise 1.5.  To retrieve the exercise files, do 

     git fetch upstream
     git checkout -b exercise-1-5_Name upstream/exercise-1-5
     git push origin --set-upstream

To compiler the code, do

     make

or, to compile manually, do for example
     
     mpif90 -c ../../mpi.f90
     mpif90 -c ../../mpi_timer.f90
     mpif90 *.o objects.f90 -o objects.x
