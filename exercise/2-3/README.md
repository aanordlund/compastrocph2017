# Exercise 2.3 #

This is the directory for Exercise 2.3.  To retrieve the exercise files, do 

    git fetch upstream
    git checkout -b exercise-2-3_YourName upstream/exercise-2-3
    git push origin --set-upstream

To compiler the code, do

    make

For exercise details, see the Absalon assignment text
