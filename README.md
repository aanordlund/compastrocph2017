# README #

### What is this repository for? ###

* This is the repository for the course Computational Astrophysics at NBI/KU
* Version 1.1
* [Markdown demo](https://bitbucket.org/tutorials/markdowndemo)

### How to set up a connection to this repository ###

* Use the "Fork" menu entry under "+" in the left-most blue ribbon to fork a server-side copy of this repository.
* Clone your own repository (not this one) to your laptop and workstations:

       `git clone git@bitbucket.org:your_userid_at_bitbbucket/CompAstroCPH_YourName`
       [ or use SourceTree menus ]

* Connect this repository as "upstream": 

       `git remote add upstream git@bitbucket.org:aanordlund/CompAstroCPH`
       [ or use SourceTree menus ]


### Contribution guidelines ###

* Work on exercises in your working copy (directory CompAstroCPH2015 on your laptop and/or workstation), pushing updates to your own bitbucket repository
* When asked to submit solutions to exercises, use the "Create pull request" menu entry under "..." here at bitbucket.org to ask the teachers to pull your solution into the main course repository

### Who do I talk to? ###

* Other students (in particular the ones that have GIT experience)
* The teachers: Troels Haugb�lle, �ke Nordlund, Jon Ramsey, Oliver Gressel, and Neil Vaytet
